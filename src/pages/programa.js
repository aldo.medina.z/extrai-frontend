import React from "react"
import PageWrapper from "../components/PageWrapper"
import Layout from "../layout"
import SEO from "../components/seo"
import { eventos } from "../assets/dummy"
import Evento from "../components/evento"
import styled from "styled-components"

const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  display: grid;
  grid-gap: 1.2rem;
  grid-template-columns: 1fr 1fr;
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`

const Programa = () => {
  return (
    <Layout>
      <SEO title="Programa" />
      <PageWrapper>
        <Wrapper>
          {eventos.map((e, i) => (
            <Evento
              slug={e.slug}
              key={`evento-${i}`}
              tipo={e.tipo}
              titulo={e.titulo}
              artista={e.artista}
              data={e.data}
              local={e.local}
            />
          ))}
        </Wrapper>
      </PageWrapper>
    </Layout>
  )
}

export default Programa
