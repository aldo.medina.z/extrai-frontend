import React from "react"
import SEO from "../components/seo"
import Layout from "../layout"
import { MemoizedEssay } from "../components/3d/essay"

const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Home" />
      <MemoizedEssay />
    </Layout>
  )
}

export default IndexPage
