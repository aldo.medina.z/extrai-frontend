import React from "react"
import PageWrapper from "../components/PageWrapper"
import Layout from "../layout"
import SEO from "../components/seo"
import styled from "styled-components"
import data from "../content/data.json"
import ArtistListItem from "../components/ArtistListItem"

const Wrapper = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
  div {
    background: #f2a17d;
    width: 100%;
  }
`
const InfoContainer = styled.div`
  border: 1px solid;
  width: calc(100% - 2.4rem);
  height: max-content;
  @media (max-width: 768px) {
    width: 100%;
    margin-right: 0;
    margin-bottom: 1.2rem;
  }
  margin-right: 1.2rem;

  div {
    width: calc(100% - 2.4rem);
    border: none;
    padding: 1.2rem;
    h2 {
      font-weight: bold;
      margin: 0;
    }
  }
`
const ListContainer = styled.div`
  border: 1px solid;
  @media (max-width: 768px) {
    height: 100%;
    overflow: auto;
  }
`
const Artistas = () => {
  return (
    <Layout>
      <SEO title="Artistas" />
      <PageWrapper>
        <Wrapper>
          <InfoContainer>
            <div>
              <h2>ARTISTAS</h2>
            </div>
          </InfoContainer>
          <ListContainer>
            {data.artists.map(({ name, slug }, index) => (
              <ArtistListItem key={`artist-${index}`} name={name} slug={slug} />
            ))}
          </ListContainer>
        </Wrapper>
      </PageWrapper>
    </Layout>
  )
}

export default Artistas
