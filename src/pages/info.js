import React, { useEffect, useState } from "react"
import styled from "styled-components"

import PageWrapper from "../components/PageWrapper"
import Layout from "../layout"
import SEO from "../components/seo"
import InteractiveMap from "../components/InteractiveMap"

const Container = styled.div`
  border-bottom: ${props => (props.border ? "1px solid" : false)};
`
const InfoContainer = styled.div`
  max-width: 600px;
  margin: 1.2rem;
  margin-top: 0;
  display: flex;
  flex-direction: column;
  p {
    margin: 0;
    margin-bottom: 1.2rem;
  }
`
function useWindowSize() {
  const isClient = typeof window === "object"

  function getSize() {
    return {
      width: isClient ? window.innerWidth : undefined,
      height: isClient ? window.innerHeight : undefined,
    }
  }

  const [windowSize, setWindowSize] = useState(getSize)

  useEffect(() => {
    if (!isClient) {
      return false
    }

    function handleResize() {
      setWindowSize(getSize())
    }

    window.addEventListener("resize", handleResize)
    return () => window.removeEventListener("resize", handleResize)
  }, []) // Empty array ensures that effect is only run on mount and unmount

  return windowSize
}

const Info = () => {
  const [mobile, setMobile] = useState(false)
  const size = useWindowSize()
  useEffect(() => {
    size.width < 1000 ? setMobile(true) : setMobile(false)
  }, [size])
  return (
    <Layout>
      <SEO title="Informações" />
      <PageWrapper bg>
        <Container border>
          <InfoContainer>
            <h2>PROJECTO EXTRAI</h2>
            <p>
              O Projeto EXTRAI, no âmbito da Programação e Desenvolvimento de
              Públicos, propõe-se como uma plataforma de criação
              transdisciplinar que encontra o seu eixo de ação num programa de
              Arte Pública de envolvimento comunitário. Um projeto a ser
              desenvolvido entre Fevereiro de 2020 e Dezembro de 2020 (ou
              Janeiro de 2021??), constitui-se como um conjunto de residências
              artísticas espaçadas no tempo, que implicam o trabalho de cinco
              residências artísticas com grupos específicos de pessoas da
              comunidade do Lousal.
            </p>
          </InfoContainer>
          <div>{mobile ? <div> mobile </div> : <InteractiveMap />}</div>
        </Container>
      </PageWrapper>
    </Layout>
  )
}

export default Info
