import React from "react"
import styled from "styled-components"

import { Container, InfoContainer } from "../components/common"
import PageWrapper from "../components/PageWrapper"
import Layout from "../layout"
import SEO from "../components/seo"
import Location from "../components/Location"
import { locais } from "../assets/dummy"

// Logos
import smfog from "../assets/smfog.png"
import cultura from "../assets/REPUBLICA-preto.png"
import dgartes from "../assets/dgartes.png"
import grandola from "../assets/grandola.png"
import outra from "../assets/outra.png"
import cieba from "../assets/cieba.png"
import azinheira from "../assets/azinheira.png"
import ciencia from "../assets/ciencia.png"
import cienciavivalousal from "../assets/cienciavivalousal.png"
import casadopovo from "../assets/casa-do-povo.png"

const TeamRows = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: calc(100% - 2.4rem);
  padding: 1.2rem;
  padding-top: 0;
  ul {
    margin: 0;
    margin-right: 20px;
    padding: 0;
    list-style: none;
    min-width: 220px;
    max-width: 280px;
    h3 {
      font-weight: bold;
      font-family: "LT22";
    }
  }
`

const teamData = [
  [{ name: "DIRECÇÃO", team: ["VITOR FURTADO", "LUÍS VITAL ALEXANDRE"] }],
  [
    { name: "DIRECÇÃO ARTÍSTICA", team: ["SÉRGIO VICENTE"] },
    {
      name: "EQUIPA TÉCNICA, MONTAGEM E PRODUÇÃO",
      team: ["INÊS BRÁS DE OLIVEIRA", "JOSÉ BICA"],
    },
    { name: "COBERTURA AUDIOVISUAL", team: ["FERNANDO FADIGAS"] },
  ],
  [
    {
      name: "DESIGN E COMUNICAÇÃO",
      team: [
        "ABEL QUENTAL",
        "ALDO MEDINA",
        "ANTÓNIO SANTIAGO",
        "GABRIELA MOURA",
      ],
    },
  ],
]

const ImageLayout = styled.div`
  padding: 1.2rem;
  display: grid;
  grid-template-columns: auto auto;

  .sponsor-types {
    font-weight: bold;
    font-family: "LT22";
  }
  .parceiros {
    grid-column: 1 / 3;
  }
  .logos {
    img {
      max-width: 180px;
      max-height: 80px;
      filter: brightness(0);
      margin: 1rem;
    }
    .smfog {
      max-height: 140px;
      margin: 0;
      margin-left: -1.4rem;
    }
    .cieba {
      filter: brightness(1);
      mix-blend-mode: multiply;
      max-width: 280px;
    }
  }
  @media (max-width: 768px) {
    grid-template-columns: auto;
    .parceiros {
      grid-column: 1 / 2;
    }
  }
`
const Projecto = () => {
  return (
    <Layout>
      <SEO title="Projecto" />
      <PageWrapper bg={true}>
        <Container border>
          <InfoContainer>
            <h2>PROJECTO EXTRAI</h2>

            <p>
              Extrair, metaforicamente, é a acção de tirar o minério de uma
              mina, mas também o acto de tirar da realidade social da aldeia
              mineira do Lousal a matéria do trabalho artístico.
            </p>
            <p>
              EXTRAI: Arte e Comunidade em Acção é um projecto de programação de
              residências artísticas numa povoação, localizada no concelho de
              Grândola, que tem a sua história ligada à exploração mineira da
              pirite desde o final do século XIX. O encerramento da mina em
              1988, veio a alterar toda a sua dinâmica sociocultural e
              demográfica provocada pelo fim da atividade mineira.
            </p>
            <p>
              São assim propostas cinco residências - com Ângela Ferreira,
              Carlos Moura, Letícia Larín, Rogério Taveira e Tiago Rocha Costa -
              estendidas ao longo de um ano, que têm as artes visuais na sua
              expressão pública como ferramenta para o trabalho social.
            </p>
            <p>
              Um projecto de António Santiago, Gabriela Moura, Inês Brás de
              Oliveira, José Bica e Sérgio Vicente financiado pela República
              Portuguesa - Cultura / Direção-Geral das Artes e pelo Município de
              Grândola, e organizado pela Sociedade Musical Fraternidade
              Operária Grandolense - Música Velha.
            </p>
          </InfoContainer>
        </Container>

        <Container>
          {locais.map(
            (
              { name, locationDetails, lat, long, description, address },
              index
            ) => (
              <Location
                key={`location-${index}`}
                name={name}
                locationDetails={locationDetails}
                lat={lat}
                long={long}
                description={description}
                address={address}
              />
            )
          )}
        </Container>
        <Container border>
          <h2 style={{ marginLeft: "1.2rem" }}>EQUIPA</h2>
          {teamData.map(row => (
            <TeamRows>
              {row.map(({ name, team }) => (
                <ul>
                  <li>
                    <h3>{name}</h3>
                  </li>
                  {team.map(p => (
                    <li>
                      <p>{p}</p>
                    </li>
                  ))}
                </ul>
              ))}
            </TeamRows>
          ))}
        </Container>
        <Container>
          <h2 style={{ marginLeft: "1.2rem" }}>APOIOS</h2>
          <ImageLayout>
            <div>
              <h3 className="sponsor-types">ORGANIZAÇÃO</h3>
              <div className="logos">
                <img className="smfog" src={smfog} alt="logo-smfog" />
              </div>
            </div>
            <div>
              <h3 className="sponsor-types">FINANCIAMENTO</h3>
              <div className="logos">
                <img src={cultura} alt="logo-cultura" />
                <img src={dgartes} alt="logo-dgartes" />
                <img src={grandola} alt="logo-grandola" />
                <img src={outra} alt="logo-outra" />
              </div>
            </div>
            <div className="parceiros">
              <h3 className="sponsor-types">PARCEIROS</h3>
              <div className="logos">
                <img className="cieba" src={cieba} alt="logo-cieba" />
                <img src={azinheira} alt="logo-azinheira" />
                <img src={ciencia} alt="logo-ciencia" />
                <img src={cienciavivalousal} alt="logo-cienciavivalousal" />
                <img src={casadopovo} alt="logo-casadopovo" />
              </div>
            </div>
          </ImageLayout>
        </Container>
      </PageWrapper>
    </Layout>
  )
}

export default Projecto
