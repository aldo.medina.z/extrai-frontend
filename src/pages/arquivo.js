import React from "react"
import PageWrapper from "../components/PageWrapper"
import Layout from "../layout"
import SEO from "../components/seo"

const Arquivo = () => {
  return (
    <Layout>
      <SEO title="Arquivo" />
      <PageWrapper>
        <h1>Arquivo</h1>
      </PageWrapper>
    </Layout>
  )
}

export default Arquivo
