import React from "react"
import styled from "styled-components"

import Layout from "../layout"
import PageWrapper from "./PageWrapper"
import back from "../assets/arrow-left.svg"

// Images
import angela from "../images/angela_640.jpg"
import carlos from "../images/carlos_640.jpg"
import leticia from "../images/leticia_640.jpg"
import rogerio from "../images/rogerio_640.jpg"
import tiago from "../images/tiago_640.jpg"
import pedro from "../images/pedro-vaz.jpg"
import { Link } from "gatsby"

const Titulo = styled.div`
  border-bottom: 1px solid;
  display: flex;
  h2 {
    padding: 0 1.2rem;
    text-transform: uppercase;
    flex: 1;
  }
`

const StyledLink = styled(Link)`
  width: 67px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-right: 1px solid #000;
`
const Contenido = styled.div`
  display: flex;
  .bio {
    flex: 1;
    padding: 1.2rem 2.4rem;
    line-height: 1.4rem;
  }
  @media (max-width: 768px) {
    flex-direction: column;
  }
`
const Img = styled.div`
  background-image: ${props => `url("${props.src}")`};
  background-position: center;
  background-size: cover;
  width: 320px;
  min-height: 320px;
  @media (max-width: 768px) {
    width: calc(100vw - 21.2px);
    height: 100vw;
    border-bottom: 1px solid;
  }
`

const ArtistPage = props => {
  const {
    pageContext: { name, bio, image, slug },
  } = props
  const getSrc = slug =>
    ({
      "tiago-rocha": tiago,
      "leticia-larin": leticia,
      "angela-ferreira": angela,
      "rogerio-taveira": rogerio,
      "carlos-moura": carlos,
      "pedro-vaz": pedro,
    }[slug])
  return (
    <Layout>
      <PageWrapper bg={true}>
        <Titulo>
          <StyledLink to="/artistas">
            <img src={back} />
          </StyledLink>
          <h2>{name}</h2>
        </Titulo>
        <Contenido>
          <Img src={getSrc(slug)} />
          <div className="bio">
            {bio.map(p => (
              <p>{p}</p>
            ))}
          </div>
        </Contenido>
      </PageWrapper>
    </Layout>
  )
}

export default ArtistPage
