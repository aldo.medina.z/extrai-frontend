import React from "react"
import styled from "styled-components"
import { useSpring, animated } from "react-spring"

const Btn = styled(animated.div)`
  height: 100%;

  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  div {
    background: #000;
    height: 20px;
    position: relative;
    width: 1px;
    transition: all 0.3s ease-in;
    &:after {
      background: #000;
      content: "";
      height: 1px;
      left: -9px;
      position: absolute;
      top: 10px;
      width: 20px;
    }
  }
`
const PlusBtn = ({ active }) => {
  const animation = useSpring({
    transform: active ? "rotate(90deg)" : "rotate(0deg)",
  })
  return (
    <Btn style={animation}>
      <div></div>
    </Btn>
  )
}

export default PlusBtn
