import styled from "styled-components"

export const Container = styled.div`
  border-bottom: ${props => (props.border ? "1px solid" : false)};
`
export const InfoContainer = styled.div`
  max-width: 600px;
  margin: 1.2rem;
  margin-top: 0;
  display: flex;
  flex-direction: column;
  p {
    margin: 0;
    margin-bottom: 1.2rem;
  }
`
