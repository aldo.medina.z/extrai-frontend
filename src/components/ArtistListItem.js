import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

const Wrapper = styled.div`
  width: calc(100% - 2.4rem);
  border-bottom: 1px solid;
  &:last-child {
    border-bottom: none;
  }
`
const Name = styled.h2`
  height: calc(100% - 2.4rem);
  text-transform: capitalize;
  padding: 1.2rem;
  margin: 0;
`

const StyledLink = styled(Link)`
  text-decoration: none;
  color: black;
`

const ArtistListItem = ({ name, slug }) => {
  return (
    <Wrapper>
      <StyledLink to={`/artistas/${slug}`}>
        <Name> {name} </Name>
      </StyledLink>
    </Wrapper>
  )
}

export default ArtistListItem
