import React, { useState } from "react"
import styled from "styled-components"
import { Link } from "gatsby"

import logo from "../../assets/logo.svg"
import Burguer from "./burguer"
import FontTools from "./FontTools"

const data = [
  { name: "projecto", slug: "projecto" },
  // { name: "programa", slug: "programa" },
  { name: "artistas", slug: "artistas" },
  // { name: "informações", slug: "info" },
]

const Wrapper = styled.div`
  position: fixed;
  top: 1.2rem;
  left: 1.2rem;
  width: 370px;
  @media (max-width: 768px) {
    top: 0.6rem;
    left: 0.6rem;
    width: calc(100% - 1.2rem);
    display: flex;
  }
`
const Header = styled.div`
  background: #f2a17d;
  border: 1px solid;
  display: flex;
  height: 75px;
  @media (max-width: 768px) {
    height: 45px;
  }
`

const LogoWrapper = styled.div`
  padding: 1.2rem;
  width: calc(100% - 2.4rem);
  display: flex;
  justify-content: flex-start;
  img {
    height: calc(75px - 2.4rem);
  }
  @media (max-width: 768px) {
    padding: 0.6rem;
    img {
      height: calc(45px - 1.2rem);
    }
  }
`
const Container = styled.div`
  transition: all 0.4s ease-in-out;
  @media (max-width: 768px) {
    border-top: 1px solid;
    top: 64px;
    left: 0.6rem;
    width: calc(100% - 1.2rem);
    position: fixed;
    transform: ${props =>
      props.open ? "translateY(0%)" : "translateY(-150%)"};
  }
`
const LinksWrapper = styled.div`
  position: relative;
  background: #f2a17d;
  border: 1px solid;
  border-top: 0;
  padding: 1.2rem;
  flex-direction: column;
  display: flex;
  @media (max-width: 768px) {
    padding: 0.6rem;
  }
`
const StyledLink = styled(Link)`
  font-size: 2.6em;
  font-family: "LT24";
  text-transform: uppercase;
  text-decoration: none;
  color: black;
  margin-bottom: 1.2rem;
  @media (max-width: 768px) {
    font-size: 2.2rem;
    margin-bottom: 0.6rem;
  }
`

const InfoWrapper = styled.div`
  background: #f2a17d;
  border: 1px solid;
  border-top: 0;
  padding: 0.6rem 1.2rem;
  display: flex;
  justify-content: space-between;
  font-size: 1rem;
  div {
    font-weight: 200;
    font-family: "LT24";
    &:first-child {
      font-weight: normal;
    }
  }
`

const Menu = ({ handleTools, showTools }) => {
  const [isOpen, setOpen] = useState(false)

  const handleClick = () => {
    setOpen(!isOpen)
  }
  return (
    <Wrapper>
      <Header>
        <LogoWrapper>
          <Link to="/">
            <img src={logo} alt="logo" />
          </Link>
        </LogoWrapper>
        <Burguer handleClick={handleClick} />
      </Header>
      <Container open={isOpen}>
        <LinksWrapper>
          {data.map((link, i) => (
            <StyledLink
              activeStyle={{ fontWeight: "bold" }}
              key={`link-${i}`}
              to={`/${link.slug}`}
            >
              {link.name}
            </StyledLink>
          ))}
        </LinksWrapper>
        <InfoWrapper>
          <div>LOUSAL 2021</div>
          <div>
            ARTE <br /> E COMUNIDADE <br />
            EM ACÇÃO
          </div>
        </InfoWrapper>
      </Container>
      {showTools && <FontTools handleTools={handleTools} />}
    </Wrapper>
  )
}

export default Menu
