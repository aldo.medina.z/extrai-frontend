import React from "react"
import styled from "styled-components"

import aplus from "../../assets/aplus.svg"
import aless from "../../assets/aless.svg"

const ToolWrapper = styled.div`
  margin-top: 0.6rem;
  width: 100%;
  display: flex;
  justify-content: flex-end;
  div {
    background: #f2a17d;
    border: 1px solid;
    height: 75px;
    width: 75px;
    margin-left: 0.6rem;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    &:first-child {
      img {
        height: 50px;
        width: 50px;
      }
    }
    img {
      height: 35px;
      width: 35px;
    }
  }
  @media (max-width: 768px) {
    margin-top: 0;
    div {
      margin-left: 0.3rem;
      height: 45px;
      width: 45px;
      &:first-child {
        img {
          height: 30px;
          width: 30px;
        }
      }
      img {
        height: 20px;
        width: 20px;
      }
    }
  }
`
const FontTools = ({ handleTools }) => {
  return (
    <ToolWrapper>
      <div onClick={() => handleTools("+")}>
        <img src={aplus} alt="increase font size" />
      </div>
      <div onClick={() => handleTools("-")}>
        <img src={aless} alt="decrease font size" />
      </div>
    </ToolWrapper>
  )
}

export default FontTools
