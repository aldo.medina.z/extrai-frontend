import React from "react"
import styled from "styled-components"

const StyledBurguer = styled.div`
  display: none;
  @media (max-width: 768px) {
    display: block;
    min-width: 45px;
    border-left: 1px solid;
    position: relative;

    input {
      display: block;
      width: 45px;
      height: 45px;
      position: absolute;
      cursor: pointer;
      opacity: 0;
      z-index: 2;
      -webkit-touch-callout: none;
    }

    span {
      display: block;
      width: calc(45px - 1rem);
      height: 3px;
      margin-bottom: 5px;
      margin-left: 0.5rem;
      position: relative;
      background: #000;
      z-index: 1;
      transform-origin: 4px 0px;
      transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1),
        background 0.5s cubic-bezier(0.77, 0.2, 0.05, 1), opacity 0.55s ease;
    }

    input:checked ~ span {
      opacity: 1;
      transform: rotate(45deg) translate(-8px, -14px);
      background: #232323;
    }

    input:checked ~ span:nth-last-child(3) {
      opacity: 0;
      transform: rotate(0deg) scale(0.2, 0.2);
    }

    input:checked ~ span:nth-last-child(2) {
      transform: rotate(-45deg) translate(-3px, 10px);
    }
  }
`

const Burguer = ({ handleClick }) => {
  return (
    <StyledBurguer onClick={handleClick}>
      <input type="checkbox" />
      <span style={{ marginTop: "13px" }}></span>
      <span style={{ transformOrigin: "0% 100%" }}></span>
      <span></span>
    </StyledBurguer>
  )
}

export default Burguer
