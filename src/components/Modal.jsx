import React, { useState, useEffect } from "react"
import styled from "styled-components"
import { animated } from "react-spring"

const ModalDiv = styled(animated.div)`
  position: fixed;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  transition: all 0.3s ease-in;
  z-index: 999;
  background: rgba(0, 0, 0, 0.4);
  .content {
    transition: 0.4s ease-out;
    max-width: 400px;
    padding: 20px;
    background: #f2a17d;
    border: 1px solid;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    h2 {
      margin: 0;
      max-width: max-content;
      cursor: pointer;
    }
  }
`
const Modal = ({ isActive }) => {
  const [show, setShow] = useState(false)
  useEffect(() => {
    setShow(true)
  }, [])
  return (
    <ModalDiv
      style={{
        opacity: show ? 1 : 0,
        display: !show && "none",
      }}
    >
      <div
        className="content"
        style={{
          transform: show ? "translateY(0)" : "translateY(-10vh)",
          opacity: show ? 1 : 0,
        }}
      >
        <h2 onClick={() => setShow(false)}>X</h2>
        <div>
          <h2>COMUNICADO</h2>
          <p>
            As atividades do projeto "EXTRAI: Arte e Comunidade em Ação”
            encontram-se neste momento suspensas até data incerta, dada a
            prospeção e contacto com a comunidade do Lousal. Contudo,
            continuamos a trabalhar para que as atividades deste projeto se
            concretizem e sejam retomadas logo que possível.
          </p>
        </div>
      </div>
    </ModalDiv>
  )
}

export default Modal
