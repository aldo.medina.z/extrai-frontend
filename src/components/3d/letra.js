import React, { useState, useEffect, useRef } from "react"
import { useRender } from "react-three-fiber"
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"

const Primitive = () => {
  const [model, setModel] = useState()

  useEffect(() => {
    new GLTFLoader().load("/letras.glb", setModel)
  }, [])

  return model ? <primitive object={model.scene} /> : null
}

const Letra = ({ pos }) => {
  const meshRef = useRef()

  useRender(() => {
    meshRef.current.position.x < -4.8
      ? (meshRef.current.position.x = 7)
      : (meshRef.current.position.x -= 0.006)
  })

  return (
    <mesh ref={meshRef} position={pos}>
      <Primitive />
    </mesh>
  )
}

export default Letra
