import React from "react"
import styled from "styled-components"
import { Canvas } from "react-three-fiber"
import * as THREE from "three"
import Rock from "./rock"
// import Letra from "./letra"

const StyledCanvas = styled(Canvas)`
  position: fixed;
  top: 0;
  height: 100%;
  width: 100%;
`

const randomDec = () => {
  let num = Math.floor(Math.random() * 100) / 100
  num *= Math.floor(Math.random() * 2) === 1 ? 1 : -1
  return num
}

const numRocks = 16
const rocks = new Array(numRocks).fill()
const xPos = Array.from({ length: 20 }, () => randomDec() * 1.8)
const yPos = Array.from({ length: 20 }, randomDec)
const zPos = Array.from(
  { length: 20 },
  () => (Math.floor(Math.random() * 150) / 100) * -1
)

const Rocks = ({ noLetters }) => {
  return (
    <StyledCanvas
      camera={{ position: [0, 0, 0.7] }}
      onCreated={({ gl }) => {
        gl.shadowMap.enabled = true
        gl.shadowMap.type = THREE.PCFSoftShadowMap
      }}
    >
      <ambientLight intensity={2} />

      <spotLight position={[2, 2, 2]} />
      <spotLight position={[0, 0, 0]} />

      {rocks.map((rock, i) => (
        <Rock
          key={`rock-${i}`}
          URL={`/pedra_1.glb`}
          pos={[xPos[i], yPos[i], zPos[i]]}
          rotX={xPos[i]}
        />
      ))}

      {rocks.map((rock, i) => (
        <Rock
          key={`rock2-${i}`}
          URL={`/pedra_2.glb`}
          pos={[xPos[i] + 4, yPos[i], zPos[i]]}
          rotX={xPos[i]}
        />
      ))}
      {/* {!noLetters && <Letra key={`letra-3d`} pos={[3, -0.5, -0.7]} />}
      {!noLetters && <Letra key={`letra-3d-2`} pos={[9, -0.5, -0.7]} />} */}
    </StyledCanvas>
  )
}

export default Rocks
