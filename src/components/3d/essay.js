import React from "react"
import styled from "styled-components"
import Rocks from "./rocks"

const Wrapper = styled.div`
  position: absolute;
  width: 100vw;
  height: 100%;
`

export const Essay = ({ noLetters }) => {
  return (
    <Wrapper>
      <Rocks noLetters={noLetters} />
    </Wrapper>
  )
}

export const MemoizedEssay = React.memo(Essay)
