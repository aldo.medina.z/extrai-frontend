import React, { useState, useEffect, useRef } from "react"
import { useRender } from "react-three-fiber"
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"
import { a, useSpring } from "react-spring/three"

const Primitive = ({ URL }) => {
  const [model, setModel] = useState()

  useEffect(() => {
    new GLTFLoader().load(URL, setModel)
  }, [])

  return model ? <primitive object={model.scene} /> : null
}

const Rock = ({ pos, rotX, URL }) => {
  const meshRef = useRef()
  const [hovered, setHovered] = useState(false)

  const props = useSpring({
    scale: hovered ? [1.5, 1.5, 1.5] : [1, 1, 1],
  })

  useRender(() => {
    meshRef.current.rotation.x += rotX * 0.005
    meshRef.current.rotation.y += pos[1] * 0.005
    meshRef.current.rotation.z += pos[2] * 0.005

    meshRef.current.position.x < -4
      ? (meshRef.current.position.x = 4)
      : (meshRef.current.position.x -= 0.006)
  })

  return (
    <a.mesh
      ref={meshRef}
      position={pos}
      onPointerOver={() => setHovered(true)}
      onPointerOut={() => setHovered(false)}
      scale={props.scale}
    >
      <Primitive URL={URL} />
    </a.mesh>
  )
}

export default Rock
