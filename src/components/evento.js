import React, { useState } from "react"
import styled from "styled-components"
import PlusBtn from "./PlusBtn"
import { Link } from "gatsby"
const Wrapper = styled(Link)`
  text-decoration: none;
  position: relative;
  background: ${props => (props.tipo === "mostra" ? "#000" : "#F2A17D")};
  color: ${props => (props.tipo === "mostra" ? "#F2A17D" : "#000")};
  height: ${props => (props.tipo === "mostra" ? "400px" : "auto")};
  border: 1px solid;
  text-transform: uppercase;
  cursor: pointer;
  transition: all 0.3s ease-in;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  grid-column: ${props =>
    props.tipo === "apresentação"
      ? "1 / span 2"
      : props.tipo === "mostra"
      ? "1 / span 2"
      : "auto"};
  @media (max-width: 768px) {
    grid-column: 1 / 2;
  }
`

const MainContent = styled.div`
  border-right: 1px solid;
  padding: 1.2rem;
  width: 100%;
  height: 100%;
  ${props =>
    props.tipo === "mostra"
      ? "display: flex; justify-content: center;border:none;"
      : ""};
  h2 {
    ${props =>
      props.tipo === "mostra"
        ? "align-self: center; margin: 0; text-align:center; "
        : ""};
    font-size: ${props => (props.tipo === "mostra" ? " 4rem" : "auto")};
    font-weight: bold;
  }
  @media (max-width: 768px) {
    padding: 0.6rem;
  }
`

const Tipo = styled.div`
  padding: 0.3rem;
  border: 1px solid;
  width: max-content;
  height: max-content;
  h4 {
    margin: 0;
  }
  @media (max-width: 768px) {
    background: ${props =>
      props.tipo === "apresentação" ? "#000" : "#F2A17D"};
    color: ${props => (props.tipo === "apresentação" ? "#F2A17D" : "#000")};
  }
`

const MetaWrapper = styled.div`
  display: flex;
  border-top: 1px solid;
`
const Meta = styled.div`
  padding: 0.3rem;
  h4 {
    margin: 0;
  }
  &:first-child {
    border-right: 1px solid;
  }
`

const Evento = ({ tipo, titulo, artista, data, local, slug }) => {
  const [hoverEvent, setHoverEvent] = useState(false)

  return (
    <Wrapper
      tipo={tipo}
      onMouseOver={() => setHoverEvent(true)}
      onMouseLeave={() => setHoverEvent(false)}
      to={slug}
    >
      <div style={{ display: "flex", height: "100%" }}>
        <MainContent tipo={tipo}>
          {tipo !== "mostra" ? (
            <Tipo tipo={tipo}>
              <h4>{tipo}</h4>
            </Tipo>
          ) : null}
          <h2>{titulo}</h2>
          <h3> {artista} </h3>
        </MainContent>
        {tipo !== "mostra" ? (
          <div style={{ width: "50px" }}>
            <PlusBtn active={hoverEvent} />
          </div>
        ) : null}
      </div>
      <MetaWrapper>
        <Meta>
          <h4>{data}</h4>
        </Meta>
        <Meta>
          <h4>{local}</h4>
        </Meta>
      </MetaWrapper>
    </Wrapper>
  )
}

export default Evento
