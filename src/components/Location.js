import React, { useState } from "react"
import styled from "styled-components"
// import PlusBtn from "./PlusBtn"
import { useSpring, animated } from "react-spring"
import { Collapse } from "react-collapse"
import mapstyle from "../assets/mapstyle"
import markerIcon from "../assets/marker.png"
import {
  GoogleMap,
  withScriptjs,
  withGoogleMap,
  Marker,
} from "react-google-maps"

const Wrapper = styled.div`
  width: 100%;
  box-sizing: border-box;
  border-bottom: ${({ isOpen }) => (isOpen ? "1px solid" : "0")};
`
const Header = styled.div`
  width: 100%;
  height: 50px;
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid;
  cursor: pointer;
  .showMoreIcon {
    font-weight: 200;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 2.5rem;
  }
  h3 {
    margin: 0.3rem 1.2rem;
  }
  div {
    &:nth-child(2) {
      width: 100%;
      min-height: 50px;
      height: 100%;
      border-left: 1px solid;
      display: flex;
      align-items: center;
    }
  }
  @media (max-width: 768px) {
    height: 100%;
    min-height: 50px;
  }
`

const Content = styled(animated.div)`
  padding: 1.2rem;
  height: calc(100% + 2.4rem);
  display: grid;
  grid-template-columns: max-content 1fr;
  p {
    margin: 0;
    padding-left: 1.2rem;
  }
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
    grid-template-rows: max-content 1fr;
    p {
      padding-top: 1.2rem;
    }
  }
`
const MapContainer = styled.div`
  height: 350px;
  border: 1px solid;
  background: "#F2A17D";
  @media (max-width: 768px) {
    height: 500px;
  }
`
const MapElement = styled.div`
  height: 100%;
  background: "#F2A17D";
`
const Tipo = styled.div`
  padding: 0.3rem;
  border: 1px solid;
  min-width: max-content;
  max-width: max-content;
  height: max-content;
`
const Location = ({
  name,
  description,
  lat,
  long,
  locationDetails,
  address,
}) => {
  const [isOpen, setOpen] = useState(false)
  // const [hoverEvent, setHoverEvent] = useState(false)

  // const collapse = useSpring({
  //   opacity: isOpen ? 1 : 0,
  // })

  // const markerLocation = {
  //   lat: lat - 0.01,
  //   lng: long + 0.01,
  // }

  const GetMap = withScriptjs(
    withGoogleMap(() => (
      <GoogleMap
        defaultZoom={14}
        defaultCenter={{ lat: lat, lng: long }}
        defaultOptions={{
          styles: mapstyle,
          fullscreenControl: false,
          streetViewControl: false,
          mapTypeControl: false,
        }}
      >
        <Marker position={{ lat: lat, lng: long }} icon={{ url: markerIcon }} />
      </GoogleMap>
    ))
  )
  return (
    <Wrapper isOpen={isOpen}>
      <Header onClick={() => setOpen(!isOpen)}>
        <div className="showMoreIcon" style={{ width: "50px" }}>
          <span>{isOpen ? "-" : "+"}</span>
        </div>
        <div>
          <h3>{name}</h3>
        </div>
      </Header>
      <Collapse
        isOpened={isOpen}
        initialStyle={{ height: 0, overflow: "hidden" }}
      >
        <Content>
          <div>
            <Tipo>
              <strong>{locationDetails}</strong>
            </Tipo>
            <p
              style={{
                paddingLeft: 0,
                paddingTop: "1.2rem",
                marginBottom: "1rem",
                maxWidth: " 220px",
              }}
            >
              <strong>Morada:</strong>
              <br />
              {address}
            </p>
          </div>
          <div>
            {!!description && (
              <p style={{ paddingBottom: "1.2rem" }}>{description} </p>
            )}
            <div style={{ paddingLeft: "1.2rem", background: "#F2A17D" }}>
              <GetMap
                googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyDddg3UVCm3AcjhougUA5AL70eSuWMX7pQ`}
                loadingElement={
                  <div
                    style={{
                      height: `100%`,
                      width: "100%",
                      background: "#F2A17D",
                    }}
                  />
                }
                containerElement={<MapContainer />}
                mapElement={<MapElement />}
              />
            </div>
          </div>
        </Content>
      </Collapse>
    </Wrapper>
  )
}

export default Location
