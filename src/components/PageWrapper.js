import React from "react"
import styled from "styled-components"
import PropTypes from "prop-types"
import { useSpring, animated } from "react-spring"

const Wrapper = styled(animated.div)`
  margin: 1.2rem;
  margin-left: calc(370px + 2.4rem);
  @media (max-width: 768px) {
    margin: 0.6rem;
    margin-top: calc(45px + 1.2rem);
    margin-left: 0.6rem;
  }
  background: ${({ bg }) => (bg ? "#F2A17D" : "")};
  border: ${({ bg }) => (bg ? "1px solid" : "0")};
`

const PageWrapper = ({ children, bg }) => {
  const props = useSpring({
    opacity: 1,
    transform: "translate3d(0%,0,0)",
    from: { opacity: 0, transform: "translate3d(100%,0,0)" },
  })
  return (
    <Wrapper bg={bg ? 1 : 0} style={props}>
      {children}
    </Wrapper>
  )
}

PageWrapper.propTypes = {
  bg: PropTypes.bool,
}

export default PageWrapper
