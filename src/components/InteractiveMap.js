import React, { useState, Fragment } from "react"
import {
  GoogleMap,
  withScriptjs,
  withGoogleMap,
  Marker,
} from "react-google-maps"
import styled, { keyframes } from "styled-components"

import mapstyle from "../assets/mapstyle"
import markerIcon from "../assets/marker.png"
import { mapSleep, mapEat, locais } from "../assets/dummy"

const Wrapper = styled.div`
  margin: 1.2rem;
  display: flex;
  border: 1px solid;
  overflow: hidden;
`

const MapContainer = styled.div`
  height: 470px;
  width: 100%;
  @media (max-width: 768px) {
    height: 500px;
  }
`
const MapElement = styled.div`
  height: 100%;
  width: 100%;
`

const FadeIn = keyframes`
  from {
    transorm: translateX(-100%);
    opacity: 0;
  }
  to {
    transorm: translateX(0);
    transform: 1;
  }
`
const Results = styled.div`
  width: 300px;
  border-right: 1px solid;
  div {
    animation: ${FadeIn} 0.3s ease-in;
    h2,
    p {
      margin: 0;
    }
    border-bottom: 1px solid;
    padding: 0.6rem;
    &:last-child {
      border: none;
    }
  }
`
const Menu = styled.ul`
  width: 100%;
  text-decoration: none;
  list-style: none;
  margin: 0;
  padding: 0;
  display: flex;
  border-bottom: 1px solid;
  li {
    margin: 0;
    padding: 0.6rem;
    cursor: pointer;
    display: flex;
    border-right: 1px solid;

    h3 {
      margin: 0;
    }
  }
`

const StyledMarker = styled(Marker)`
  animation: ${FadeIn} 0.3s ease-in;
  &:hover {
    transform: scale(1.3, 1.3);
  }
`

const GetMap = withScriptjs(
  withGoogleMap(({ center, arr, onClick }) => (
    <GoogleMap
      defaultZoom={14}
      defaultCenter={{ lat: center.lat, lng: center.long }}
      defaultOptions={{
        styles: mapstyle,
        fullscreenControl: false,
        streetViewControl: false,
        mapTypeControl: false,
      }}
    >
      {arr.map(({ lat, long, name, address, description }, i) => (
        <StyledMarker
          key={`marker-${i}`}
          position={{ lat: lat, lng: long }}
          icon={{ url: markerIcon }}
          onClick={() => onClick(name, address, description)}
        />
      ))}
    </GoogleMap>
  ))
)
const menuList = [
  { name: "Locais", arr: locais },
  { name: "Onde Comer", arr: mapEat },
  { name: "Onde Ficar", arr: mapSleep },
]
const center = { lat: 38.0314822, long: -8.4254086 }

const InteractiveMap = () => {
  const [activeMap, setMap] = useState(mapEat)
  const [activeSite, setActiveSite] = useState(false)
  const handleMarkerClick = (name, address, description) =>
    setActiveSite({ name, address, description })
  return (
    <Wrapper>
      <Results>
        {activeSite ? (
          <Fragment>
            <div>
              <h2>{activeSite.name}</h2>
            </div>
            <div>
              <p>
                <strong> {activeSite.address}</strong>
              </p>
            </div>
            <div>
              <p>{activeSite.description}</p>
            </div>
          </Fragment>
        ) : (
          <div
            style={{
              height: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <p> Escolha um local >></p>
          </div>
        )}
      </Results>

      <div style={{ width: "100%" }}>
        <Menu>
          {menuList.map(({ name, arr }, i) => (
            <li key={`interacitvemenu-${i}`} onClick={() => setMap(arr)}>
              <h3>{name}</h3>
            </li>
          ))}
        </Menu>
        <GetMap
          onClick={handleMarkerClick}
          arr={activeMap}
          center={center}
          googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyDddg3UVCm3AcjhougUA5AL70eSuWMX7pQ`}
          loadingElement={
            <div style={{ height: `100%`, background: "#F2A17D" }} />
          }
          containerElement={<MapContainer />}
          mapElement={<MapElement />}
        />
      </div>
    </Wrapper>
  )
}

export default InteractiveMap
