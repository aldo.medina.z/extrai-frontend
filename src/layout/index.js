import React, { useState, useEffect } from "react"
import { createGlobalStyle } from "styled-components"
import bg from "../assets/bgpattern.svg"
import { fonts } from "./fonts"
import Menu from "../components/menu/menu"
import SEO from "../components/seo"
import Modal from "../components/Modal"
import { useLocation } from "@reach/router"
// import { useLoader } from "react-three-fiber"

export const Colors = {
  yellow: "#F2A17D",
}

const GlobalStyle = createGlobalStyle`
  ${fonts}  
  body {
    margin: 0;
    background-color: ${Colors.yellow};
    background-size: 1rem;
    background-image: url(${bg});
    background-repeat: repeat;
    font-family: "LT22";
    height: 100%;
    width: 100%;
  }
  h1, h2, h3, h4, h5 {
    font-family: "LT24";   
    
  }
  html, canvas, #___gatsby, #gatsby-focus-wrapper, .tl-edges, .tl-wrapper {
    width: 100%;
    height: 100%;
  }  
  h2 {
    font-size: ${({ factor }) => 1.5 * factor}em;
    font-weight: normal;
    transition: all 0.3s ease-in;
  }
 
  p, h4 {
    font-size: ${({ factor }) => factor}em;
    transition: all 0.3s ease-in;
  }
  .ReactCollapse--collapse {  
    transition: height 500ms;
  }
`
export default ({ children }) => {
  const [factor, setFactor] = useState(1)
  const [itsHome, setItsHome] = useState(false)
  const location = useLocation()
  useEffect(() => {
    location.pathname === "/" && setItsHome(true)
  }, [])
  const handleTools = change => {
    let refactor = 0.3
    change === "+" && factor + refactor < 1.9
      ? setFactor(factor + refactor)
      : change === "-" && factor - refactor > 0.99
      ? setFactor(factor - refactor)
      : setFactor(factor)
  }
  return (
    <React.Fragment>
      <SEO title="Home" />
      <GlobalStyle factor={factor} />
      {children}
      <Menu handleTools={handleTools} showTools={!itsHome} />
      {!!itsHome && <Modal isActive={itsHome} />}
    </React.Fragment>
  )
}
