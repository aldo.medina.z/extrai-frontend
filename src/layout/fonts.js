export const fonts = `@font-face {
  font-family: "LT22";
  src: url("/fonts/lunchtype22/lunchtype22-light-webfont.woff")   format("woff");
  font-weight: 200;
}   
@font-face {
  font-family: "LT22";
  src: url("/fonts/lunchtype22/lunchtype22-regular-webfont.woff")   format("woff");
  font-weight: normal;
}   
@font-face {
  font-family: "LT22";
  src: url("/fonts/lunchtype22/lunchtype22-medium-webfont.woff")   format("woff");
  font-weight: bold;
}   
@font-face {
  font-family: "LT24";
  src: url("/fonts/lunchtype24/lunchtype24-light-expanded-webfont.woff")   format("woff");
  font-weight: 200;
}   
@font-face {
  font-family: "LT24";
  src: url("/fonts/lunchtype24/lunchtype24-regular-expanded-webfont.woff")   format("woff");
  font-weight: normal;
}   
@font-face {
  font-family: "LT24";
  src: url("/fonts/lunchtype24/lunchtype24-medium-expanded-webfont.woff")   format("woff");
  font-weight: bold;
}   `
