export const locais = [
  {
    name: "Centro Comunitário do Lousal",
    locationDetails: "Lousal / Alentejo / Portugal",
    lat: 38.0227822,
    long: -8.4314086,
    address: "Minas do Lousal",
    description: "",
  },
  {
    name: "Sociedade Musical de Fraternidade Operária Grandolense",
    locationDetails: "Grândola / Alentejo / Portugal",
    lat: 38.1753125,
    long: -8.56768,
    address:
      "Rua Dr. Evaristo de Sousa Gago (junto à Praça D. Jorge) - Grândola",
    description: "",
  },
  {
    name: "Centro de Ciência Viva do Lousal",
    locationDetails: "Lousal / Alentejo / Portugal",
    lat: 38.035609,
    long: -8.425964,
    address: "Avenida Frédéric Velge. 7570-006",
    description: "",
  },
]

export const artist = [
  { name: "Jenna Pittman", slug: "Jenna-Pittman" },
  { name: "Rickey Alvarez", slug: "Rickey-Alvarez" },
  { name: "Deanna Reed", slug: "Deanna-Reed" },
  { name: "Rodolfo Romero", slug: "Rodolfo-Romero" },
  { name: "Gerard Lloyd", slug: "Gerard-Lloyd" },
  { name: "Sonja Fuller", slug: "Sonja-Fuller" },
  { name: "Connie Farmer", slug: "Connie-Farmer" },
  { name: "Dominick Briggs", slug: "Dominick-Briggs" },
  { name: "Roy Watson", slug: "Roy-Watson" },
  { name: "Perry Leonard", slug: "Perry-Leonard" },
  { name: "Otis Baldwin", slug: "Otis-Baldwin" },
  { name: "Leticia Mckinney", slug: "Leticia-Mckinney" },
  { name: "Shelia Manning", slug: "Shelia-Manning" },
  { name: "Rosemary Ward", slug: "Rosemary-Ward" },
  { name: "Ervin Hopkins", slug: "Ervin-Hopkins" },
  { name: "Sheri Peterson", slug: "Sheri-Peterson" },
  { name: "Robin Phillips", slug: "Robin-Phillips" },
  { name: "Jane Dean", slug: "Jane-Dean" },
  { name: "Darla Mclaughlin", slug: "Darla-Mclaughlin" },
  { name: "Laura Hale", slug: "Laura-Hale" },
  { name: "Marlon Beck", slug: "Marlon-Beck" },
  { name: "Gerardo George", slug: "Gerardo-George" },
  { name: "Luz Bradley", slug: "Luz-Bradley" },
  { name: "Glenda Williams", slug: "Glenda-Williams" },
  { name: "Alton Burns", slug: "Alton-Burns" },
]

export const eventos = [
  {
    id: 1,
    tipo: "residência",
    titulo: "Olfato. Sessões de trabalho comunitário e discussão",
    artista: "Ângela Ferreira",
    data: "18/03/2020",
    local: "Centro de Ciência Viva do Lousal",
    slug: "Olfato-Sessões-de-trabalho-comunitário-e-discussão",
  },
  {
    id: 2,
    tipo: "residência",
    titulo: "Olfato. Sessões de trabalho comunitário e discussão",
    artista: "Ângela Ferreira",
    data: "18/03/2020",
    local: "Centro de Ciência Viva do Lousal",
    slug: "Olfato-Sessões-de-trabalho-comunitário-e-discussão",
  },
  {
    id: 3,
    tipo: "apresentação",
    titulo: "Olfato. Sessões de trabalho comunitário e discussão",
    artista: "Ângela Ferreira",
    data: "18/03/2020",
    local: "Centro de Ciência Viva do Lousal",
    slug: "Olfato-Sessões-de-trabalho-comunitário-e-discussão",
  },
  {
    id: 4,
    tipo: "residência",
    titulo: "Olfato. Sessões de trabalho comunitário e discussão",
    artista: "Ângela Ferreira",
    data: "18/03/2020",
    local: "Centro de Ciência Viva do Lousal",
    slug: "Olfato-Sessões-de-trabalho-comunitário-e-discussão",
  },
  {
    id: 5,
    tipo: "residência",
    titulo: "Olfato. Sessões de trabalho comunitário e discussão",
    artista: "Ângela Ferreira",
    data: "18/03/2020",
    local: "Centro de Ciência Viva do Lousal",
    slug: "Olfato-Sessões-de-trabalho-comunitário-e-discussão",
  },
  {
    id: 6,
    tipo: "residência",
    titulo: "Olfato. Sessões de trabalho comunitário e discussão",
    artista: "Ângela Ferreira",
    data: "18/03/2020",
    local: "Centro de Ciência Viva do Lousal",
    slug: "Olfato-Sessões-de-trabalho-comunitário-e-discussão",
  },
  {
    id: 7,
    tipo: "mostra",
    titulo: "MOSTRA FINAL",
    data: "18/03/2020",
    local: "Centro de Ciência Viva do Lousal",
    slug: "Olfato-Sessões-de-trabalho-comunitário-e-discussão",
  },
]

export const mapSleep = [
  {
    name: "Centro Comunitário do Lousal",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. A provident ad consectetur hic quibusdam nihil dolore reiciendis iste at aliquid?",
    address: "Rua Almirante Nunes da Gama #666",
    lat: 38.0277822,
    long: -8.4314086,
  },
  {
    name: "Centro Comunitário do Lousal",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. A provident ad consectetur hic quibusdam nihil dolore reiciendis iste at aliquid?",
    address: "Rua Almirante Nunes da Gama #666",
    lat: 38.0277822,
    long: -8.4314086,
  },
  {
    name: "Centro de Ciência Viva do Lousal",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. A provident ad consectetur hic quibusdam nihil dolore reiciendis iste at aliquid?",
    address: "Rua Almirante Nunes da Gama #666",
    lat: 38.035609,
    long: -8.425964,
  },
]
export const mapEat = [
  {
    name: "Tasca",
    description:
      "Lorem ipsum dolor si adipisicing elit. A provident ad consectetur hic quibusdam nihil dolore reiciendis iste at aliquid?",
    address: "Rua Almirante  da Gama #666",
    lat: 38.0207822,
    long: -8.4314086,
  },
  {
    name: "Padaria",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. A provident ad consectetur hic quibusdam nihil dolore reiciendis iste at aliquid?",
    address: "Rua Almirante Nunes da  #666",
    lat: 38.1853125,
    long: -8.53768,
  },
  {
    name: "Peixeria",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. A provident ad consectetur hic quibusdam nihil dolore reiciendis iste at aliquid?",
    address: "Rua  Nunes da Gama #666",
    lat: 38.035609,
    long: -8.445964,
  },
]
