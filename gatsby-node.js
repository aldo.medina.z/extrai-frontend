const path = require(`path`)
const fs = require("fs")

exports.createPages = ({ actions }) => {
  const { createPage } = actions
  const data = JSON.parse(
    fs.readFileSync("./src/content/data.json", { encoding: "utf-8" })
  )
  const artistTemplate = path.resolve(`src/components/ArtistPage.jsx`)
  // Create artist pages
  data.artists.forEach(page => {
    createPage({
      path: `artistas/${page.slug}`,
      component: artistTemplate,
      context: {
        ...page,
      },
    })
  })
  // Create event pages
}
